import unittest
import requests
import json
import random
from firebase_get_data import get_data_from_firebase, url
import arduino_data
import time

class TestUpdateDataOnFirebase(unittest.TestCase):

    def test_update_data_on_firebase(self):
        # Fetch the initial data
        climate_control_data, _, _ = get_data_from_firebase()
        initial_fan_speed = climate_control_data.get('fan_speed', {}).get('integerValue', None)

        # Generate a new fan speed
        new_fan_speed = initial_fan_speed
        while new_fan_speed == initial_fan_speed:
            new_fan_speed = {'integerValue': str(random.randint(1, 5))}  # Generate a random integer between 1 and 100

        # Update the fan speed
        climate_control_data['fan_speed'] = new_fan_speed

        # Create a new dictionary that follows the structure of the data in your Firestore document
        new_data = {
            'fields': {
                'climate_control': {
                    'mapValue': {
                        'fields': climate_control_data
                    }
                }
            }
        }

        # Convert the dictionary to a JSON string
        new_data_json = json.dumps(new_data)

        # Send the updated data back to Firebase
        # Add a FieldMask to specify which fields to update
        response = requests.patch(url, data=new_data_json, params={'updateMask.fieldPaths': 'climate_control'})

        # Fetch the updated data
        updated_climate_control_data, _, _ = get_data_from_firebase()
        updated_fan_speed = updated_climate_control_data.get('fan_speed', {}).get('integerValue', None)

        # Assert that the fan speed has been updated
        self.assertNotEqual(initial_fan_speed, updated_fan_speed)

        if response.ok:
            print("Data updated successfully.")
        else:
            print("Error updating data:", response.status_code, response.content)

        # Generate a dummy PDU with the new fan speed for testing the Arduino process_climate_control function
        pdu = [0x20, 1, 1, 0, 1, 1, 1, 0, int(new_fan_speed['integerValue'])]
        time.sleep(5)
        # Get the fan speed from arduino_data using the process_climate_control function
        arduino_fan_speed = arduino_data.process_climate_control(pdu)

        # Assert that the fan speeds are equal
        self.assertEqual(new_fan_speed['integerValue'], str(arduino_fan_speed))

if __name__ == "__main__":
    unittest.main()