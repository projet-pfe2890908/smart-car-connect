
import requests



# Firebase Configuration
project_id = 'smartcar-connct'  # Your Firebase project ID
collection_name = 'cars'
document_id = 'tLzhEbMXi5mNy6yDrOO8'  # Your Firestore document ID
api_key = 'YOUR_API_KEY'

# URL for accessing the Firestore document
url = f'https://firestore.googleapis.com/v1/projects/{project_id}/databases/(default)/documents/{collection_name}/{document_id}?key={api_key}'

# Function to retrieve data from Firebase
def get_data_from_firebase():
    response = requests.get(url)
    if response.ok:
        data = response.json().get('fields', {})
        climate_control_data = data.get('climate_control', {}).get('mapValue', {}).get('fields', {})
        key_status = data.get('key_status', {}).get('booleanValue', None)
        ambient_light_control_data = data.get('ambiant_light_control', {}).get('mapValue', {}).get('fields', {})
        seat_control_data = data.get('seat_controls', {}).get('mapValue', {}).get('fields', {})
        return climate_control_data, key_status, ambient_light_control_data
    else:
        print("Error retrieving data from Firebase:", response.status_code, response.content)
        return {}, False, {}, {}

