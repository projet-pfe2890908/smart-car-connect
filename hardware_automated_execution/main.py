

import time
from pywinauto import Application
import subprocess


# Start Proteus 8 and open a specific project
app = Application(backend='uia').start("C:/Program Files (x86)/Labcenter Electronics/Proteus 8 Professional/BIN/PDS.EXE")

time.sleep(5)
app.connect(title="UNTITLED - Proteus 8 Professional - Home Page",visible_only=False)
openproject = app.UntitledProteus8ProfessionalHomePage.child_window(title="Open Project", control_type="Button")
openproject.click()
app.UntitledProteus8ProfessionalHomePage.child_window(title="Look in:", auto_id="1137", control_type="ComboBox").child_window(title="Open", auto_id="DropDown", control_type="Button").click()
fileacces = app.UntitledProteus8ProfessionalHomePage.child_window(title="hardware_simulation_last", control_type="ListItem")
fileacces.click_input(double=True)

projectaccess = app.UntitledProteus8ProfessionalHomePage.child_window(title="smartcarconnect", control_type="ListItem")
projectaccess.click_input(double=True)
app.connect(title="smartcarconnect - Proteus 8 Professional - Schematic Capture")
time.sleep(3)
find=app.smartcarconnectProteus8ProfessionalSchematicCapture.menu_select("Edit->Find/Edit Component E ")
component= app.smartcarconnectProteus8ProfessionalSchematicCapture.child_window(title="Component:", auto_id="257", control_type="Edit")
component.type_keys("ARD1")
okbutton=app.smartcarconnectProteus8ProfessionalSchematicCapture.child_window(title="OK", auto_id="1", control_type="Button")
okbutton.click()

# Integrate finding XCODE folder path
hex_file_path ="C:\\Users\\AHMED\\Documents\\XCODE\\firmware.hex"
programfile=app.smartcarconnectProteus8ProfessionalSchematicCapture.child_window(title="Program File:", auto_id="3585", control_type="Image")
programfile.click_input()
programfile.type_keys(hex_file_path)
okbutton2=app.smartcarconnectProteus8ProfessionalSchematicCapture.child_window(title="OK", auto_id="1", control_type="Button")
okbutton2.click_input()

find=app.smartcarconnectProteus8ProfessionalSchematicCapture.menu_select("Edit->Find/Edit Component E ")
component= app.smartcarconnectProteus8ProfessionalSchematicCapture.child_window(title="Component:", auto_id="257", control_type="Edit")
component.type_keys("ARD2")
okbutton=app.smartcarconnectProteus8ProfessionalSchematicCapture.child_window(title="OK", auto_id="1", control_type="Button")
okbutton.click()

# Integrate finding YCODE folder path

hex_file_pathY ="C:\\Users\\AHMED\\Documents\\YCODE\\firmware.hex"
programfile=app.smartcarconnectProteus8ProfessionalSchematicCapture.child_window(title="Program File:", auto_id="3585", control_type="Image")
programfile.click_input()
programfile.type_keys(hex_file_path)
okbutton2=app.smartcarconnectProteus8ProfessionalSchematicCapture.child_window(title="OK", auto_id="1", control_type="Button")
okbutton2.click_input()



app2=Application(backend='uia').start("C:\\Program Files\\Electronic Team\\Virtual Serial Port Driver 10\\vspdpro.exe", timeout=100)
time.sleep(5)
app2.connect(title="Virtual Serial Port Driver Pro",visible_only=False)
app2.virtualserialportdriverpro.child_window(title="Delete all", control_type="MenuItem").click_input()
app2.VirtualSerialPortDriverPro.child_window(title="Yes", control_type="Button").click()
app2.VirtualSerialPortDriverActivation.menu_select("New bundle->Pair")
app2.VirtualSerialPortDriverActivation.child_window(title="COM1", auto_id="PART_EditableTextBox", control_type="Edit").type_keys("COM1")
app2.VirtualSerialPortDriverActivation.child_window(title="COM2", auto_id="PART_EditableTextBox", control_type="Edit").type_keys("COM2")
app2.VirtualSerialPortDriverActivation.child_window(title="COM1", auto_id="PART_EditableTextBox", control_type="Edit").select().type_keys('{TAB}')
app2.VirtualSerialPortDriverActivation.type_keys('{ENTER}')
app2.VirtualSerialPortDriverActivation.AccessButton.click_input()
proteusinput = app2.VirtualSerialPortDriverActivation.child_window(title="...", control_type="Button").click()
app2.VirtualSerialPortDriverActivation.child_window(title="This PC (pinned)", control_type="TreeItem").click_input()
app2.VirtualSerialPortDriverActivation.child_window(title="OS (C:)", auto_id="0", control_type="ListItem").click_input(double=True)
app2.VirtualSerialPortDriverActivation.child_window(title="\r", auto_id="SearchEditBox", control_type="Edit").type_keys("Program{SPACE}Files ")
time.sleep(2)
app2.VirtualSerialPortDriverActivation.child_window(title="Program Files (x86)", control_type="ListItem").click_input(double=True)
app2.VirtualSerialPortDriverActivation.child_window(title="\r", auto_id="SearchEditBox",control_type="Edit").type_keys("Labcenter{SPACE}Electronics ")
time.sleep(2)
app2.VirtualSerialPortDriverActivation.child_window(title="Labcenter Electronics", control_type="ListItem").click_input(double=True)
app2.VirtualSerialPortDriverActivation.child_window(title="Proteus 8 Professional", control_type="ListItem").click_input(double=True)
app2.VirtualSerialPortDriverActivation.child_window(title="BIN", control_type="ListItem").click_input(double=True)
app2.VirtualSerialPortDriverActivation.child_window(title="\r", auto_id="SearchEditBox",control_type="Edit").type_keys("PDS")
time.sleep(2)
app2.VirtualSerialPortDriverActivation.child_window(title="PDS", control_type="ListItem").click_input(double=True)
app2.VirtualSerialPortDriverActivation.child_window(title="Save", control_type="Button").click()
app2.VirtualSerialPortDriverActivation.child_window(title="Create", control_type="Button").click()
time.sleep(2)

app2.VirtualSerialPortDriverActivation.menu_select("New bundle->Pair")
app2.VirtualSerialPortDriverActivation.child_window(title="COM3", auto_id="PART_EditableTextBox", control_type="Edit").type_keys("COM9")
app2.VirtualSerialPortDriverActivation.child_window(title="COM4", auto_id="PART_EditableTextBox", control_type="Edit").type_keys("COM10")
app2.VirtualSerialPortDriverActivation.child_window(title="COM9", auto_id="PART_EditableTextBox", control_type="Edit").select().type_keys('{TAB}')
app2.VirtualSerialPortDriverActivation.type_keys('{ENTER}')
app2.VirtualSerialPortDriverActivation.AccessButton.click_input()
proteusinput = app2.VirtualSerialPortDriverActivation.child_window(title="...", control_type="Button").click()
app2.VirtualSerialPortDriverActivation.child_window(title="This PC (pinned)", control_type="TreeItem").click_input()
app2.VirtualSerialPortDriverActivation.child_window(title="OS (C:)", auto_id="0", control_type="ListItem").click_input(double=True)
app2.VirtualSerialPortDriverActivation.child_window(title="\r", auto_id="SearchEditBox", control_type="Edit").type_keys("Program{SPACE}Files ")
time.sleep(2)
app2.VirtualSerialPortDriverActivation.child_window(title="Program Files (x86)", control_type="ListItem").click_input(double=True)
app2.VirtualSerialPortDriverActivation.child_window(title="\r", auto_id="SearchEditBox",control_type="Edit").type_keys("Labcenter{SPACE}Electronics ")
time.sleep(2)
app2.VirtualSerialPortDriverActivation.child_window(title="Labcenter Electronics", control_type="ListItem").click_input(double=True)
app2.VirtualSerialPortDriverActivation.child_window(title="Proteus 8 Professional", control_type="ListItem").click_input(double=True)
app2.VirtualSerialPortDriverActivation.child_window(title="BIN", control_type="ListItem").click_input(double=True)
app2.VirtualSerialPortDriverActivation.child_window(title="\r", auto_id="SearchEditBox",control_type="Edit").type_keys("PDS")
time.sleep(2)
app2.VirtualSerialPortDriverActivation.child_window(title="PDS", control_type="ListItem").click_input(double=True)
app2.VirtualSerialPortDriverActivation.child_window(title="Save", control_type="Button").click()
app2.VirtualSerialPortDriverActivation.child_window(title="Create", control_type="Button").click()



arduino_data_process = subprocess.Popen(["python", "arduino_data.py"])



runsystem=app.smartcarconnectProteus8ProfessionalSchematicCapture.menu_select("Debug->Run Simulation F12")
time.sleep(10)
arduino_data_process.terminate()
app.smartcarconnectProteus8ProfessionalSchematicCapture.print_control_identifiers()


