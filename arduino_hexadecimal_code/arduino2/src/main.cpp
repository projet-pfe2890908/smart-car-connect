#include<Arduino.h>
void setup() {
  Serial.begin(115200); // Initialize serial communication
}
void processClimateControl();
void processAmbientLight();
void processSeatControl();

void loop() {
  if (Serial.available() >= 9) { // Check if enough bytes are available for a complete PDU
    byte startMarker = Serial.read();
    if (startMarker == 0x20) {
      // Climate control settings received
      processClimateControl();
    }
    else if (startMarker == 0x21) {
      // Ambient light settings received
      processAmbientLight();
    }
    else if (startMarker == 0x22) {
      // Seat control settings received
      processSeatControl();
    }
  }
}

void processClimateControl() {
  byte pdu[9]; // Corrected to include the start marker
  // Read climate control data including start marker
  pdu[0] = 0x20;
  for (int i = 1; i < 9; i++) {
    while (!Serial.available()) {} // Wait for data
    pdu[i] = Serial.read();
  }

  // Extract data from PDU
  bool auto_program_active = pdu[1];
  bool maximum_cooling_active = pdu[2];
  bool cooling_function_active = pdu[3];
  bool rear_window_defroster_active = pdu[4];
  bool defrost_windows_active = pdu[5];
  bool system_power_status = pdu[6];
  float temperature_setting = (float)pdu[7] / 10; // Convert to float with one decimal place
  int fan_speed = pdu[8];

  // Print received climate control data
  Serial.println("[ARDUINO] Received climate control data:");
  Serial.println("[ARDUINO] Auto Program Active: " + String(auto_program_active));
  Serial.println("[ARDUINO] Maximum Cooling Active: " + String(maximum_cooling_active));
  Serial.println("[ARDUINO] Cooling Function Active: " + String(cooling_function_active));
  Serial.println("[ARDUINO] Rear Window Defroster Active: " + String(rear_window_defroster_active));
  Serial.println("[ARDUINO] Defrost Windows Active: " + String(defrost_windows_active));
  Serial.println("[ARDUINO] System Power Status: " + String(system_power_status));
  Serial.println("[ARDUINO] Temperature Setting: " + String(temperature_setting, 1)); // Print with one decimal place
  Serial.println("[ARDUINO] Fan Speed: " + String(fan_speed));
}

void processAmbientLight() {
  byte pdu[6]; // Corrected to include the start marker
  // Read ambient light data including start marker
  pdu[0] = 0x21;
  for (int i = 1; i < 6; i++) {
    while (!Serial.available()) {} // Wait for data
    pdu[i] = Serial.read();
  }

  // Extract data from PDU
  bool ambient_light_status = pdu[1] != 0;
  int ambient_light_color_red = pdu[2];
  int ambient_light_color_green = pdu[3];
  int ambient_light_color_blue = pdu[4];
  int ambient_light_brightness = pdu[5];

  // Print received ambient light data
  Serial.println("[ARDUINO] Received ambient light data:");
  Serial.println("[ARDUINO] Ambient Light Status: " + String(ambient_light_status));
  Serial.println("[ARDUINO] Color Red: " + String(ambient_light_color_red));
  Serial.println("[ARDUINO] Color Green: " + String(ambient_light_color_green));
  Serial.println("[ARDUINO] Color Blue: " + String(ambient_light_color_blue));
  Serial.println("[ARDUINO] Brightness: " + String(ambient_light_brightness));
}

void processSeatControl() {
  byte pdu[9]; // Corrected to include the start marker
  // Read seat control data including start marker
  pdu[0] = 0x22;
  for (int i = 1; i < 9; i++) {
    while (!Serial.available()) {} // Wait for data
    pdu[i] = Serial.read();
  }

  // Extract data from PDU
  int forward_backward_adjustment = (pdu[1] << 8) | pdu[2]; // Combine two bytes
  int recline_angle_adjustment = (pdu[3] << 8) | pdu[4]; // Combine two bytes
  int height_adjustment = (pdu[5] << 8) | pdu[6]; // Combine two bytes
  bool seat_heating = pdu[7] & 0x01;
  bool seat_ventilation = pdu[7] & 0x02;
  bool occupancy_sensor = pdu[7] & 0x04;
  bool seat_belt_status = pdu[7] & 0x08;

  // Print received seat control data
  Serial.println("[ARDUINO] Received seat control data:");
  Serial.println("[ARDUINO] Forward/Backward Adjustment: " + String(forward_backward_adjustment) + " cm");
  Serial.println("[ARDUINO] Recline Angle Adjustment: " + String(recline_angle_adjustment) + " degrees");
  Serial.println("[ARDUINO] Height Adjustment: " + String(height_adjustment) + " cm");
  Serial.println("[ARDUINO] Seat Heating: " + String(seat_heating ? "On" : "Off"));
  Serial.println("[ARDUINO] Seat Ventilation: " + String(seat_ventilation ? "On" : "Off"));
  Serial.println("[ARDUINO] Occupancy Sensor: " + String(occupancy_sensor ? "Occupied" : "Unoccupied"));
  Serial.println("[ARDUINO] Seat Belt Status: " + String(seat_belt_status ? "Fastened" : "Unfastened"));
}
