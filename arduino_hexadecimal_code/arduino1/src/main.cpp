#include<Arduino.h>
const int LED_PIN = 13; // Built-in LED pin for indicating data reception

void setup() {
  pinMode(LED_PIN, OUTPUT);
  Serial.begin(115200);
  Serial.println("Arduino 1 is ready to receive data...");
}

void loop() {
  if (Serial.available()) { // Check if any bytes are available
    digitalWrite(LED_PIN, HIGH); // Turn on LED to indicate data reception

    byte receivedData[8];
    int bytesReceived = Serial.readBytes(receivedData, 8); // Read available bytes from serial

    // Display received data in hexadecimal format
    Serial.print("Received data: ");
    for (int i = 0; i < bytesReceived; i++) {
      Serial.print("0x");
      Serial.print(receivedData[i], HEX);
      Serial.print(" ");
    }
    Serial.println();

    // Parse received data and display key_status
    bool keyStatus = receivedData[7] != 0;

    Serial.print("Key Status: ");
    Serial.println(keyStatus ? "True" : "False");

    digitalWrite(LED_PIN, LOW); // Turn off LED
  } else {
    delay(1000); // Delay before retrying
  }
}
